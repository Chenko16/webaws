package servlets;


import cephManaging.CephClient;
import com.amazonaws.services.s3.model.Bucket;
import cephManaging.AWS;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

@WebServlet("/upload")
//@MultipartConfig(maxFileSize = 1024*1024*1024,maxRequestSize = 1024*1024*1024,fileSizeThreshold = 8*1024*1024)
public class Upload extends HttpServlet {

    /**
     * Выводит на консоль имя параметра и значение
     * @param item
     */
    private void processFormField(FileItem item) {
        System.out.println(item.getFieldName()+"="+item.getString());
    }

    /**
     * Сохраняет файл на сервере, в папке upload.
     * Сама папка должна быть уже создана.
     *
     * @throws Exception
     */
    private void processUploadedFile(FileItem item, AWS aws, String bucket_name) throws Exception {
        File uploadetFile = null;
        String path = null;
        String object_name = item.getName();
        final Random random = new Random();
        //выбираем файлу имя пока не найдём свободное
        do{
            path = getServletContext().getRealPath("/upload/" +random.nextInt() + object_name);
            File dir = new File(getServletContext().getRealPath("/upload/"));
            dir.mkdir();
            uploadetFile = new File(path);
        }while(uploadetFile.exists());
        //создаём файл
        uploadetFile.createNewFile();
        //записываем в него данные
        item.write(uploadetFile);
        aws.multypartUploadObject(bucket_name, object_name, path);
        for (File myFile : new File(getServletContext().getRealPath("/upload/")).listFiles())
            if (myFile.isFile()){
                myFile.delete();
            }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String bucket_name = request.getParameter("act");
        //проверяем является ли полученный запрос multipart/form-data
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (!isMultipart) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        // Создаём класс фабрику
        DiskFileItemFactory factory = new DiskFileItemFactory();

        // Максимальный буфера данных в байтах,
        // при его привышении данные начнут записываться на диск во временную директорию
        // устанавливаем один мегабайт

        // устанавливаем временную директорию
        File tempDir = (File)getServletContext().getAttribute("javax.servlet.context.tempdir");
        factory.setRepository(tempDir);

        //Создаём сам загрузчик
        ServletFileUpload upload = new ServletFileUpload(factory);
        try {
            List items = upload.parseRequest(request);
            Iterator iter = items.iterator();

            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                if (item.isFormField()) {
                    //если принимаемая часть данных является полем формы
                    processFormField(item);
                } else {
                    //в противном случае рассматриваем как файл;
                    processUploadedFile(item, CephClient.client, bucket_name);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            //return;
        }

        RequestDispatcher dispatcher;
        dispatcher = request.getRequestDispatcher("/menu?act=" +bucket_name);
        dispatcher.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        super.doGet(request, response);
    }
}