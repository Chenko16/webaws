package zip;

import org.apache.commons.io.FilenameUtils;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Iterator;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.*;

public class IZip {
    public static void compress(String path) {

        try {
            File input = new File(path);
            String extension = FilenameUtils.getExtension(path);

            BufferedImage image = ImageIO.read(input);

            //File compressedImageFile = new File(path + '.' + extension);
            File compressedImageFile = new File(path);
            OutputStream os = new FileOutputStream(compressedImageFile);

            Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName(extension);
            ImageWriter writer = (ImageWriter) writers.next();

            ImageOutputStream ios = ImageIO.createImageOutputStream(os);
            writer.setOutput(ios);

            ImageWriteParam param = writer.getDefaultWriteParam();

            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            if (extension.equals("jpg"))
                param.setCompressionQuality(0.5f);
            writer.write(null, new IIOImage(image, null, null), param);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
