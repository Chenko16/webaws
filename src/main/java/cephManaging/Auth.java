package cephManaging;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import java.io.*;
import java.security.Principal;
import java.security.cert.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Auth {




    public static String getHostName(X509Certificate certificate) {
        try {
            // compare to subjectAltNames if dnsName is present
            Collection<List<?>> subjAltNames = certificate.getSubjectAlternativeNames();
            if (subjAltNames != null) {
                for (Iterator<List<?>> itr = subjAltNames.iterator(); itr.hasNext();) {
                    List<?> next = itr.next();
                    if (((Integer) next.get(0)).intValue() == 2) {
                        return ((String) next.get(1));
                    }
                }
            }
        }
        catch (CertificateException e) {
            e.printStackTrace();
        }
        // else check against common name in the subject field
        Principal subject = certificate.getSubjectX500Principal();
        return getCommonName(subject);
    }

    private static String getCommonName(Principal subject) {
        try {
            LdapName name = new LdapName(subject.getName());
            for (Rdn rdn : name.getRdns()) {
                if ("cn".equalsIgnoreCase(rdn.getType())) {
                    return (String) rdn.getValue();
                }
            }
        }
        catch (InvalidNameException e) {
            e.printStackTrace();
        }
        return null;
    }

}
