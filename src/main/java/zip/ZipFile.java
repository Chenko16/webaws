package zip;

import eu.medsea.mimeutil.MimeUtil2;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ZipFile {
    public static String compress(String path){
        String resPath = path;
        try {
            File file = new File(path);

            String mimeType = getMimeType(path);
            System.out.println(mimeType);

            Pattern image = Pattern.compile("image/.*");
            Pattern audio = Pattern.compile("audio/.*");
            Matcher m = image.matcher(mimeType);
            if(m.matches()){
                IZip.compress(path);
            }else
            {
                m = audio.matcher(mimeType);
                if(m.matches()){
                    AudioConverter.convertToAAC(path);
                }
                else{
                    if(!mimeType.equals("application/x-dosexec")){
                        XZip.compress(path);
                        resPath = path+".cmp";
                    }
                }


            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
        return resPath;
    }

    public static String decompress(String path){
        String resPath = path;
        try {
            File file = new File(path);

            String mimeType = getMimeType(path);

            Pattern image = Pattern.compile("image/.*");
            Pattern audio = Pattern.compile("audio/.*");
            Matcher m = image.matcher(mimeType);
            if(!m.matches()){
                m = audio.matcher(mimeType);
                if(m.matches()){
                    AudioConverter.convertFromAAC(path);
                }
                else{
                    if(!mimeType.equals("application/x-dosexec")){
                        XZip.decompress(path);
                        resPath = path+".ucp";
                    }
                }

            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return resPath;
    }


    public  static String getMimeType(String fileName) {
        MimeUtil2 mimeUtil = new MimeUtil2();
        mimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.MagicMimeMimeDetector");
        String mimeType = MimeUtil2.getMostSpecificMimeType(mimeUtil.getMimeTypes(fileName)).toString();
        return mimeType;
    }


}
