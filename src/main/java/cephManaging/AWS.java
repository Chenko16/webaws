package cephManaging;

import encryption.AESenc;
import encryption.MyAES.MyAES;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import zip.ZipFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AWS {
    static AmazonS3 client;
    String cryptoAlgoritm;

    public AWS(BasicAWSCredentials credentials, String endpoint) {
        // здесь я инициирую подключение к своему серверу
        client = new AmazonS3Client(credentials);
        client.setEndpoint(endpoint);
        cryptoAlgoritm = "AES";
    }

    public AWS(BasicAWSCredentials credentials, String endpoint, String algo) {
        // здесь я инициирую подключение к своему серверу
        client = new AmazonS3Client(credentials);
        client.setEndpoint(endpoint);
        cryptoAlgoritm = algo;
    }


    public void createBucket(String bucket_name) {
        // здесь я проверяю есть ли корзина с таким именем
        // если нет, то создаю её
        if (client.doesBucketExist(bucket_name)) {
            System.out.format("Bucket %s already exists.\n", bucket_name);
        } else {
            try {
                client.createBucket(bucket_name);
            } catch (AmazonS3Exception e) {
                System.err.println(e.getErrorMessage());
            }
        }
    }


    private String encrypt(String path) {
        try {
            switch (cryptoAlgoritm) {
                case "AES": {
                    AESenc.encryptFile(path);
                    break;
                }
                default: {
                    AESenc.encryptFile(path);
                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return path + ".encrypted";
    }


    private String decrypt(String path) {
        try {
            switch (cryptoAlgoritm) {
                case "AES": {
                    AESenc.decryptFile(path);
                    break;
                }
                default: {
                    AESenc.decryptFile(path);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return path + ".decrypted";
    }

    public List<Bucket> listBuckets() {
        // здесь я вывожу все корзины для моего клиента
        return client.listBuckets();
    }


    public void deleteBuckets(String bucket_name) {
        // здесь я вывожу все корзины для моего клиента
        if (client.doesBucketExist(bucket_name))
            client.deleteBucket(bucket_name);
    }


    public void createFolder(String bucketName, String folderName) {
        try{
            client.putObject(bucketName, folderName, "");
        }catch (AmazonServiceException e){
            System.err.println(e.getErrorMessage());
            System.exit(1);
        }
    }


    private void addToList(List<CephObject> objectList, S3ObjectSummary object, TreeSet<String> usedFolders) {
        boolean isFolder = false;
        Pattern folderPat = Pattern.compile("(^[ A-Za-zА-ЯЁа-яё0-9_.-]*)/([ A-Za-zА-ЯЁа-яё0-9/._-]*)");
        Matcher m = folderPat.matcher(object.getKey());
        String folderName = null;
        if (m.matches()) {
            isFolder = true;
            folderName = m.group(1);
            if (!usedFolders.contains(folderName)) {
                CephObject obj = new CephObject(folderName, null, null, null, null, true);
                objectList.add(obj);
                usedFolders.add(folderName);
            }
        } else {
            String units = "bytes";
            Float size = (float)object.getSize();
            if(object.getSize() > 1024*1024){
                if(object.getSize() > 1024*1024*1024){
                    size = (float) Math.round(((float)object.getSize())/(1024*1024*1024)*100)/100;
                    units = "GB";
                }
                else{
                    size = (float) Math.round(((float)object.getSize())/(1024*1024)*100)/100;
                    units = "MB";
                }

            }
            else{
                if(object.getSize() > 1024){
                    size = (float) Math.round(((float)object.getSize())/1024*100)/100;
                    units = "KB";
                }
            }
            CephObject obj = new CephObject(object.getKey(), size.toString(), units, object.getOwner().getId(), object.getLastModified().toString(), false);
            objectList.add(obj);
        }
    }


    public List<CephObject> listObject(String bucket_name) {
        Pattern folderPat = Pattern.compile("(^[ A-Za-zА-ЯЁа-яё0-9_.-]*)/([ A-Za-zА-ЯЁа-яё0-9/_.-]*)");
        Matcher m = folderPat.matcher(bucket_name);
        String bucketName = null;
        String folderPath = null;
        if (m.matches()) {
            bucketName = m.group(1);
            folderPath = m.group(2);
        } else {
            bucketName = bucket_name;
        }
        TreeSet<String> usedFolders = new TreeSet<String>();
        List<CephObject> objectList = new ArrayList<CephObject>();
        ListObjectsV2Result result = client.listObjectsV2(bucketName);
        List<S3ObjectSummary> objects = result.getObjectSummaries();
        if (folderPath == null) {
            for (S3ObjectSummary object : objects) {
                addToList(objectList, object, usedFolders);
            }
        } else {
            for (S3ObjectSummary object : objects) {
                Pattern isInFolder = Pattern.compile(folderPath + "/([ A-Za-zА-ЯЁа-яё0-9/._-]*)");
                m = isInFolder.matcher(object.getKey());
                if (m.matches() && !object.getKey().equals(folderPath+"/")) {
                    object.setKey(m.group(1));
                    addToList(objectList, object, usedFolders);
                }
            }
        }
        objectList.sort(CephObject.compare);
        return objectList;
    }

    public String downloadObject(String bucket_name, String object_name, String path) {
        String decPath = null;
        String ucpPath = null;
        try {
            S3Object o = client.getObject(bucket_name, object_name);
            S3ObjectInputStream s3is = o.getObjectContent();
            FileOutputStream fos = new FileOutputStream(new File(path));
            byte[] read_buf = new byte[1024];
            int read_len = 0;
            while ((read_len = s3is.read(read_buf)) > 0) {
                fos.write(read_buf, 0, read_len);
            }
            s3is.close();
            fos.close();
            decPath = decrypt(path);
            ucpPath = ZipFile.decompress(decPath);
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            System.exit(1);
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (IOException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ucpPath;
    }


    public void multypartUploadObject(String bucket_name, String object_name, String path) {
        if (client.doesObjectExist(bucket_name, object_name)) {
            deleteObject(bucket_name, object_name, false);
        }
        File file = null;
        long contentLength = 0;
        long partSize = 5 * 1024 * 1024; // Set part size to 5 MB.

        try {
            String cmpPath = zip.ZipFile.compress(path);
            String encPath = encrypt(cmpPath);
            file = new File(encPath);
            contentLength = file.length();

            // Create a list of ETag objects. You retrieve ETags for each object part uploaded,
            // then, after each individual part has been uploaded, pass the list of ETags to
            // the request to complete the upload.
            List<PartETag> partETags = new ArrayList<PartETag>();

            // Initiate the multipart upload.
            InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(bucket_name, object_name);
            InitiateMultipartUploadResult initResponse = client.initiateMultipartUpload(initRequest);

            // Upload the file parts.
            long filePosition = 0;
            for (int i = 1; filePosition < contentLength; i++) {
                // Because the last part could be less than 5 MB, adjust the part size as needed.
                partSize = Math.min(partSize, (contentLength - filePosition));

                // Create the request to upload a part.
                UploadPartRequest uploadRequest = new UploadPartRequest()
                        .withBucketName(bucket_name)
                        .withKey(object_name)
                        .withUploadId(initResponse.getUploadId())
                        .withPartNumber(i)
                        .withFileOffset(filePosition)
                        .withFile(file)
                        .withPartSize(partSize);

                // Upload the part and add the response's ETag to our list.
                UploadPartResult uploadResult = client.uploadPart(uploadRequest);
                partETags.add(uploadResult.getPartETag());

                filePosition += partSize;
            }

            // Complete the multipart upload.
            CompleteMultipartUploadRequest compRequest = new CompleteMultipartUploadRequest(bucket_name,
                    object_name, initResponse.getUploadId(), partETags);
            client.completeMultipartUpload(compRequest);
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteObject(String bucket_name, String object_name, boolean isFolder) {
        try {
            if(isFolder){
                Pattern p = Pattern.compile("(^[ A-Za-zА-ЯЁа-яё0-9_.-]*)/([ A-Za-zА-ЯЁа-яё0-9/_.-]*)");
                Matcher m = p.matcher(bucket_name);
                String buck = null;
                String prefix = null;
                if(m.matches()){
                    buck = m.group(1);
                    prefix = m.group(2)+"/";
                }
                else{
                    buck = bucket_name;
                    prefix = "";
                }
                List<S3ObjectSummary> list = client.listObjects(buck).getObjectSummaries();
                Pattern inFolder = Pattern.compile(prefix+object_name+"([ A-Za-zА-ЯЁа-яё0-9/_.-]*)");
                for(S3ObjectSummary object: list){
                    m = inFolder.matcher(object.getKey());
                    if(m.matches()) {
                        client.deleteObject(buck, object.getKey());
                    }
                }
            }
            else
                client.deleteObject(bucket_name, object_name);
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            System.exit(1);
        }
    }

}
