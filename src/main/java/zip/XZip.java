package zip;

import org.tukaani.xz.LZMA2Options;
import org.tukaani.xz.XZInputStream;
import org.tukaani.xz.XZOutputStream;

import java.io.*;

public class XZip {

    public static void compress(String path){

        try {
            final File sourceFile = new File(path);
            final File compressed = new File(path+".cmp");

            FileInputStream inFile = new FileInputStream(sourceFile);
            FileOutputStream outFile = new FileOutputStream(compressed);

            LZMA2Options option = new LZMA2Options();

            option.setPreset(9);

            XZOutputStream out = new XZOutputStream(outFile, option);
            byte[] buf = new byte[1024*256];
            int size;
            while ((size = inFile.read(buf)) != -1) {
                out.write(buf, 0, size);
            }

            out.finish();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void decompress(String path){
        try {
            final File compressed = new File(path);
            final File unCompressed = new File(path+".ucp");

            FileInputStream inFile = new FileInputStream(compressed);
            FileOutputStream outFile = new FileOutputStream(unCompressed);

            XZInputStream in = new XZInputStream(inFile);

            byte[] buf = new byte[1024*256];
            int size;
            while ((size = in.read(buf)) != -1) {
                outFile.write(buf, 0, size);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
