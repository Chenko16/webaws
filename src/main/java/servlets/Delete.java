package servlets;

import cephManaging.CephClient;
import cephManaging.CephObject;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import cephManaging.AWS;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;


@WebServlet("/delete")
public class Delete extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String error = "";
        String bucket_name = request.getParameter("act");
        String object_name = request.getParameter("obj");
        boolean isFolder = false;
        if(request.getParameter("isFolder").equals("true"))
            isFolder = true;

        CephClient.client.deleteObject(bucket_name, object_name, isFolder);

        RequestDispatcher dispatcher;
        dispatcher = request.getRequestDispatcher("/menu?act=" +bucket_name);
        dispatcher.forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        super.doGet(request, response);
    }
}