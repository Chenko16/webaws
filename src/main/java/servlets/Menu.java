package servlets;

import cephManaging.CephClient;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import cephManaging.AWS;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;



@WebServlet("/menu")
public class Menu extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher;

        List<Bucket> list = CephClient.client.listBuckets();
        request.setAttribute("listBuckets", list);
        request.setAttribute("activeBucket",request.getParameter("act"));
        request.setAttribute("listObjects", CephClient.client.listObject(request.getParameter("act")));

        dispatcher = request.getRequestDispatcher("/cloud.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");

        RequestDispatcher dispatcher;

        List<Bucket> list = CephClient.client.listBuckets();
        request.setAttribute("listBuckets", list);
        request.setAttribute("activeBucket",request.getParameter("act"));
        request.setAttribute("listObjects", CephClient.client.listObject(request.getParameter("act")));

        dispatcher = request.getRequestDispatcher("/cloud.jsp");
        dispatcher.forward(request, response);

    }
}