package cephManaging;

import com.amazonaws.auth.BasicAWSCredentials;

public class CephClient {
    static public AWS client= null;

    static public  void setClient(BasicAWSCredentials credentials, String endpoint){
        client = new AWS(credentials, endpoint);
    }

    static public boolean isExist(){
        if(client == null){
            return false;
        }
        return true;
    }

}
