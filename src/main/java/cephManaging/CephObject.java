package cephManaging;

import java.util.Comparator;

public class CephObject {
    private String key;
    private String size;
    private String units;
    private String owner;
    private String modified;
    private boolean isFolder;


    public CephObject(String key, String size, String units, String owner, String modified, boolean isFolder) {
        this.key = key;
        this.size = size;
        this.units = units;
        this.owner = owner;
        this.modified = modified;
        this.isFolder = isFolder;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public boolean isFolder() {
        return isFolder;
    }

    public void setFolder(boolean folder) {
        isFolder = folder;
    }

    public static final Comparator<CephObject> compare = new Comparator<CephObject>() {
        @Override
        public int compare(CephObject o1, CephObject o2) {
            if(!o1.isFolder() & o2.isFolder())
                return 1;
            return -1;
        }
    };
}
