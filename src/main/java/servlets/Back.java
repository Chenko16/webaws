package servlets;

import cephManaging.CephClient;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@WebServlet("/back")
public class Back extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String bucket_name = request.getParameter("act");

        Pattern folderPat = Pattern.compile("(^[ A-Za-zА-ЯЁа-яё0-9_./-]*)/([ A-Za-zА-ЯЁа-яё0-9_.-]*)");
        Matcher m = folderPat.matcher(bucket_name);
        if(m.matches()) {
            bucket_name = m.group(1);
        }

        RequestDispatcher dispatcher;
        dispatcher = request.getRequestDispatcher("/menu?act=" +bucket_name);
        dispatcher.forward(request, response);
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        super.doGet(request, response);
    }
}
