package servlets;


import cephManaging.CephClient;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.model.Bucket;
import cephManaging.AWS;
import cephManaging.Auth;
import dataBase.DataBase;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.cert.X509Certificate;
import java.util.List;

@WebServlet("/")
public class Authentication extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        super.doPost(request, response);
    }

    @Override


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        X509Certificate[] cert = (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");
        int last = cert.length - 1;
        String userName = Auth.getHostName(cert[last]);


        String endpoint = "http://192.168.19.130:9000";

        DataBase base = new DataBase("AWS","sa","8x725x32250");
        BasicAWSCredentials credentials = base.getUserCredentials(userName);
        if(credentials!=null){
            CephClient.setClient(credentials, endpoint);
            List<Bucket> list = CephClient.client.listBuckets();
            CephClient.client.createBucket(userName);
            RequestDispatcher dispatcher;
            dispatcher = request.getRequestDispatcher("/menu?act=" +userName);
            dispatcher.forward(request, response);
        }
        else{
            out.print("HTTP/1.1 403 Forbidden");
        }

    }
}