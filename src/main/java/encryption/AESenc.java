package encryption;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

public class AESenc {
    private static final String ALGO = "AES";
    private static final byte[] keyValue =
            new byte[]{'A','F','i','s','h',' ','O','u','t','O','f','W','a','t','e','r'}; //must be 16 bytes

    /**
     * Encrypt a string with AES algorithm.
     *
     * @param data is a string
     * @return the encrypted string
     */
    public static byte[] encrypt(byte[] data) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data);
        return encVal;
    }

    public static void encryptFile(String path) throws  Exception {
        try(FileInputStream fin=new FileInputStream(path);
            FileOutputStream fos=new FileOutputStream(path+".encrypted"))
        {
            int i=-1;
            int bufSize = 1024*256;
            byte[] read = new byte[bufSize];
            byte[] enc;
            while((i=fin.read(read))!=-1){
                byte[] buf = new byte[i];
                System.arraycopy(read, 0, buf, 0, i);
                enc = encrypt(buf);
                fos.write(enc);
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Decrypt a string with AES algorithm.
     *
     * @param encryptedData is a string
     * @return the decrypted string
     */
    public static byte[] decrypt(byte[] encryptedData) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decValue = c.doFinal(encryptedData);
        return decValue;
    }

    public static void decryptFile(String path) throws  Exception{
        try(FileInputStream fin=new FileInputStream(path);
            FileOutputStream fos=new FileOutputStream(path+".decrypted"))
        {
            int i=-1;
            int bufSize = 1024*256+16;
            byte[] read = new byte[bufSize];
            byte[] dec;
            while((i=fin.read(read))!=-1){
                byte[] buf = new byte[i];
                System.arraycopy(read,0,buf,0,i);
                dec = decrypt(buf);
                fos.write(dec);
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Generate a new encryption key.
     */
    private static Key generateKey() throws Exception {
        return new SecretKeySpec(keyValue, ALGO);
    }

}
