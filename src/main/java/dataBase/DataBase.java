package dataBase;

import com.amazonaws.auth.BasicAWSCredentials;

import java.sql.*;

public class DataBase {
    Connection con = null;
    String connectionUrl;

    public DataBase() {
        connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                "databaseName=AWS;user=sa;password=8X725x32250";
    }

    public DataBase(String databaseName, String user, String passwprd) {
        connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                "databaseName="+databaseName+";user="+user+";password="+passwprd;
    }

    public BasicAWSCredentials getUserCredentials(String username) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl);
            Statement stmt = null;
            ResultSet rs = null;
            if (username != null) {
                // Create and execute an SQL statement that returns some data.
                String SQL = "use AWS select * from Users Where UserName = '" + username + "'";
                stmt = con.createStatement();
                rs = stmt.executeQuery(SQL);
                if (rs.next()) {
                    String accessKey = rs.getString("AccessKey");
                    String secretKey = rs.getString("SecretKey");
                    BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
                    return credentials;
                }
                else {
                    return null;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void insertUser(String userName, String accessKey, String secretKey) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl);
            Statement stmt = null;
            ResultSet rs = null;
            if(userName == null || accessKey == null || secretKey ==null )
                throw new SQLException("Can`t insert");
            String SQL = "INSERT INTO Users VALUES ('"+
                    userName+"','"+accessKey+"', '"+secretKey+"')";
            stmt = con.createStatement();
            stmt.execute(SQL);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}