<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Ченко
  Date: 22.07.2018
  Time: 13:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <script>
        $(document).ready(function () { // вся мaгия пoсле зaгрузки стрaницы
            $('a#go').click(function (event) { // лoвим клик пo ссылки с id="go"
                event.preventDefault(); // выключaем стaндaртную рoль элементa
                $('#overlay').fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку
                    function () { // пoсле выпoлнения предъидущей aнимaции
                        $('#modal_form')
                            .css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;
                            .animate({opacity: 1, top: '50%'}, 200); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз
                    });
            });
            /* Зaкрытие мoдaльнoгo oкнa, тут делaем тo же сaмoе нo в oбрaтнoм пoрядке */
            $('#modal_close, #overlay').click(function () { // лoвим клик пo крестику или пoдлoжке
                $('#modal_form')
                    .animate({opacity: 0, top: '45%'}, 200,  // плaвнo меняем прoзрaчнoсть нa 0 и oднoвременнo двигaем oкнo вверх
                        function () { // пoсле aнимaции
                            $(this).css('display', 'none'); // делaем ему display: none;
                            $('#overlay').fadeOut(400); // скрывaем пoдлoжку
                        }
                    );
            });
        });
    </script>

    <script type="text/javascript">
        function CheckFilesize() {
            var file = document.getElementById("fileInput").files[0];
            if (file.size > 1024 * 1024 * 1024 * 5) {
                document.getElementById("error").innerHTML = "Error: file`s size must be less than 5 GB"
                return false;
            }
            else {
                document.getElementById("error").innerHTML = " "
                return true;
            }
        }
    </script>

    <script type="text/javascript">
        function showBar() {
            var bar = document.getElementById("bar");
            var close = document.getElementById("close");
            close.style.display = 'none';
            bar.style.display = 'block';

        }
    </script>

    <link rel="stylesheet" type="text/css" href="style.css"/>
    <title>Menu</title>
    <style>
        body {
            margin: 0;
            padding: 0;
        }

        table {
            padding: 0;
            line-height: 2;
            top: 0;
            table-layout: fixed;
            overflow: hidden;
        }

        a {
            text-decoration: none; /* Отменяем подчеркивание у ссылки */
        }

        form {
            margin: 0; /* Убираем отступы */
        }

        div {
            font-size: 125%
        }

        .modalDialog {
            position: fixed;
            font-family: Arial, Helvetica, sans-serif;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background: rgba(0, 0, 0, 0.8);
            z-index: 99999;
            -webkit-transition: opacity 400ms ease-in;
            -moz-transition: opacity 400ms ease-in;
            transition: opacity 400ms ease-in;
            display: none;
            pointer-events: none;
        }

        .modalDialog:target {
            display: block;
            pointer-events: auto;
        }

        .modalDialog > div {
            width: 400px;
            position: relative;
            margin: 10% auto;
            padding: 5px 20px 13px 20px;
            border-radius: 10px;
            background: #fff;
            background: -moz-linear-gradient(#fff, #999);
            background: -webkit-linear-gradient(#fff, #999);
            background: -o-linear-gradient(#fff, #999);

        }

        .close {
            background: #606061;
            color: #FFFFFF;
            line-height: 25px;
            position: absolute;
            right: -12px;
            text-align: center;
            top: -10px;
            width: 24px;
            text-decoration: none;
            font-weight: bold;
            -webkit-border-radius: 12px;
            -moz-border-radius: 12px;
            border-radius: 12px;
            -moz-box-shadow: 1px 1px 3px #000;
            -webkit-box-shadow: 1px 1px 3px #000;
            box-shadow: 1px 1px 3px #000;
        }

        .close:hover {
            background: gray;
        }

    </style>
</head>
<body link="black" alink="black" vlink="black">

<table width=100% height=100% border="0" align="left" cellspacing="0" cellpadding="0">
    <tr>
        <td width="18%" align="center" bgcolor="#262626" style="line-height: 1.3;">
            <c:forEach items="${listBuckets}" var="bucket">
                <a href="${pageContext.request.contextPath}/menu?act=${bucket.getName()}">
                    <div style="color:white">${bucket.getName()}</div>
                </a>
            </c:forEach>
        </td>
        <td bgcolor=#F5F5F5 align="left" valign="top">
            <h3>&nbsp;&nbsp;&nbsp;&nbsp;${activeBucket}/</h3>
            <table width=100% height=2% border="0" cellspacing="0" cellpadding="0" bgcolor=#F5F5F5>
                <tr>
                    <td width="8%" align="center">
                        <form method="post" vertical-align="bottom"
                              action="${pageContext.request.contextPath}/back?act=${activeBucket}">
                            <input type="submit" name="Delete" value="   Back   ">
                        </form>
                    </td>
                    <td width="74%" align="center"></td>
                    <td width="8%" align="center">
                        <a href="#addFolder">
                            <button type="button">Add folder</button>
                        </a>
                    </td>
                    <td width="8%" align="center">
                        <a href="#uploadFile">
                            <button type="button">Upload file</button>
                        </a>
                    </td>
                    <td width="2%" align="center"></td>
                </tr>
            </table>
            <br>
            <table width=100% height=3% border="0" cellspacing="0" cellpadding="0" bgcolor=#F5F5F5>
                <tr>
                    <td width="30%" align="center"><h4>Name</h4></td>
                    <td width="10%" align="center"><h4>Size</h4></td>
                    <td width="10%" align="center"><h4>Owner</h4></td>
                    <td width="32%" align="center"><h4>Last Modified</h4></td>
                    <td width="8%" align="center"></td>
                    <td width="8%" align="center"></td>
                    <td width="2%" align="center"></td>
                </tr>
            </table>

            <table width=100% height=5% border="0" cellspacing="0" cellpadding="0" bgcolor=#F5F5F5>
                <c:forEach items="${listObjects}" var="object">
                    <c:choose>
                        <c:when test="${object.isFolder() == true}">
                            <tr>
                                <td width="30%" align="center"><a
                                        href="${pageContext.request.contextPath}/menu?act=${activeBucket}/${object.getKey()}">
                                    <div style="font-size: 100%">${object.getKey()}/...</div>
                                </a></td>
                                <td width="10%" align="center"></td>
                                <td width="10%" align="center"></td>
                                <td width="32%" align="center"></td>
                                <td width="8%" align="center"></td>
                                <td width="8%" align="center">
                                    <form method="post" vertical-align="bottom"
                                          action="${pageContext.request.contextPath}/delete?obj=${object.getKey()}&act=${activeBucket}&isFolder=true">
                                        <input type="submit" name="Delete" value="   Delete   ">
                                    </form>
                                </td>
                                <td width="2%" align="center"></td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <td width="30%" align="center">${object.getKey()}</td>
                                <td width="10%" align="center">${object.getSize()} ${object.getUnits()}</td>
                                <td width="10%" align="center">${object.getOwner()}</td>
                                <td width="32%" align="center">${object.getModified()}</td>
                                <td width="8%" align="center">
                                    <form method="post" vertical-align="bottom"
                                          action="${pageContext.request.contextPath}/download?obj=${object.getKey()}&act=${activeBucket}">
                                        <input type="submit" name="Download" value="Download">
                                    </form>
                                </td>
                                <td width="8%" align="center">
                                    <form id="delform" method="post" vertical-align="bottom"
                                          action="${pageContext.request.contextPath}/delete?obj=${object.getKey()}&act=${activeBucket}&isFolder=true">
                                        <input type="submit" name="Delete" value="   Delete   ">
                                    </form>

                                </td>
                                <td width="2%" align="center"></td>

                            </tr>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </table>

            <br>
        </td>
    </tr>
</table>


<div id="addFolder" class="modalDialog">
    <div>
        <a href="#close" title="Закрыть" class="close">X</a>
        <form method="post"
              action="${pageContext.request.contextPath}/addFolder?act=${activeBucket}">
            <p align="center"><input type="text" name="data" size="27%">
                <input type="submit" value="  Add  "></p>
        </form>
    </div>
</div>


<div id="uploadFile" class="modalDialog">
    <div>
        <a href="#close" title="Закрыть" class="close" id="close">X</a>
        <form method="post" enctype="multipart/form-data" onsubmit="return CheckFilesize();"
              action="${pageContext.request.contextPath}/upload?act=${activeBucket}">
            <p align="center"><input type="file" name="data">
                <input type="submit" onclick="showBar()" value="Upload"></p>
            <p align="center" id="bar" style="display: none"><img src="images/progress.gif" alt="Progress bar"/></p>
            <p id="error"></p>
        </form>
    </div>
</div>


</body>
</html>