package zip;
import it.sauronsoftware.jave.*;
import org.apache.commons.io.FilenameUtils;

import java.io.File;

public class AudioConverter {


    public static void convertToAAC(String path) {
        String extension = FilenameUtils.getExtension(path);

        Encoder encoder = new Encoder();

        MultimediaInfo sourseInfo = null;

        try {
            sourseInfo = encoder.getInfo(new File(path));
        } catch (EncoderException e) {
            e.printStackTrace();
        }

        AudioAttributes audio = new AudioAttributes();
        audio.setBitRate(sourseInfo.getAudio().getBitRate()*1000);
        EncodingAttributes atributes = new EncodingAttributes();
        atributes.setFormat("aac");
        atributes.setAudioAttributes(audio);

        try {
            encoder.encode(new File(path), new File(path+".aac"), atributes);
        } catch (EncoderException e) {
            e.printStackTrace();
        }
    }

    public static void convertFromAAC(String path) {
        String fileName = FilenameUtils.getFullPath(path)+FilenameUtils.getBaseName(path);
        String extension = FilenameUtils.getExtension(fileName);

        Encoder encoder = new Encoder();

        MultimediaInfo sourseInfo = null;

        try {
            sourseInfo = encoder.getInfo(new File(path));
        } catch (EncoderException e) {
            e.printStackTrace();
        }

        AudioAttributes audio = new AudioAttributes();
        audio.setBitRate(sourseInfo.getAudio().getBitRate()*1000);
        EncodingAttributes atributes = new EncodingAttributes();
        atributes.setFormat(extension);
        atributes.setAudioAttributes(audio);

        try {
            encoder.encode(new File(path), new File(fileName), atributes);
        } catch (EncoderException e) {
            e.printStackTrace();
        }
    }

}
